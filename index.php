<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Test 2</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.4.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.1/css/all.min.css">
    <link rel="stylesheet" href="./main.css?v<?= time() ?>">
</head>
<body>
<nav class="navbar navbar-default">
    <div class="container">
        <div class="navbar-header">
            <a class="navbar-brand" href="index.php">Index</a>
        </div>
    </div>
</nav>

<div class="container">
    <div class="page-header">
        <h2 class="text-center">File upload form <span class="uploading"><i class="fas fa-spinner fa-spin"></i></span></h2>
    </div>

    <div class="progress">
        <div class="progress-bar active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
    </div>

    <form class="form-horizontal" id="upload-form" action="action.php">
        <div class="form-group text-center">
            <label for="file">
                <i class="fas fa-file-upload fa-5x"></i>
            </label>
            <input type="file" id="file" class="hidden">
        </div>

        <div class="form-group text-center">
            <button type="submit" class="btn btn-default">Send</button>
        </div>
    </form>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.4.1/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.1/js/all.min.js"></script>
<script src="./main.js?v<?= time() ?>"></script>
</body>
</html>
