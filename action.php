<?php

const __ROOT__        = __DIR__ . DIRECTORY_SEPARATOR;
const __UPLOADS_DIR__ = __ROOT__ . 'files' . DIRECTORY_SEPARATOR;

if (empty($_SERVER['HTTP_X_REQUESTED_WITH']) || strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) !== 'xmlhttprequest') {
    header('Location: /index.php');
    exit;
}

if (empty($_POST) || empty($_POST['uniqueFileID']) || empty($_POST['fileExtension'])) {
    header('HTTP/1.1 400 Bad Request');
    exit;
}

/**
 * @param string $fileName
 *
 * @return int
 */
function checkFileSize(string $fileName): int
{
    $fullPathFile = __UPLOADS_DIR__ . $fileName;
    if (!file_exists($fullPathFile)) {
        return 0;
    }

    return filesize($fullPathFile) ?: 0;
}

/**
 * @param string $fileName
 * @param string $fileContent
 *
 * @return int
 */
function appendFile(string $fileName, string $fileContent)
{
    $fullFileName    = __UPLOADS_DIR__ . $fileName;
    $contentForWrite = explode('base64,', $fileContent)[1];
    $fileHandle      = fopen($fullFileName, 'ab');
    fwrite($fileHandle, base64_decode($contentForWrite));
    fclose($fileHandle);

    return json_encode([
        'isOk'             => true,
        'uploadedFileSize' => filesize($fullFileName),
    ]);
}

$uniqueFileID  = $_POST['uniqueFileID'];
$fileExtension = $_POST['fileExtension'];
$checkFileSize = $_POST['checkFileSize'] ?? false;
$fileContent   = $_POST['fileContent'] ?? false;

if (isset($fileExtension) && preg_match('/^[A-z]+$/', $fileExtension) === 1) {
    $uniqueFileID .= ".{$fileExtension}";
}

if ($checkFileSize) {
    echo checkFileSize($uniqueFileID);
    exit;
}

if ($fileContent) {
    echo appendFile($uniqueFileID, $fileContent);
    exit;
}
