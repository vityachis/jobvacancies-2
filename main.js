const TYPE_ALERT_DANGER = 'danger';
const TYPE_ALERT_SUCCESS = 'success';
const TYPE_ALERT_INFO = 'info';
const TYPE_ALERT_PRIMARY = 'primary';
const TYPE_ALERT_WARNING = 'warning';

const SIZE_CONTENT = 1000;
const FORM_ELEM = $('#upload-form');

let addAlert = (type, content) => {
    let alertTypeReplace = '%ALERT_TYPE%';
    let alertContentReplace = '%ALERT_CONTENT%';

    let btnElem = FORM_ELEM.find('button');
    let fileLabelElem = FORM_ELEM.find('label');
    let fileInputElem = FORM_ELEM.find('input[type="file"]');

    let alertTemplate = '<div class="alert alert-' + alertTypeReplace + ' alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + alertContentReplace + '</div>';

    let alertElem = alertTemplate
        .replace(alertTypeReplace, type)
        .replace(alertContentReplace, content);

    $('body > .container').prepend(alertElem);

    btnElem.removeClass('disabled');
    btnElem.removeAttr('title');

    fileLabelElem.removeClass('disabled');
    fileLabelElem.removeAttr('title');

    fileInputElem.removeClass('disabled');
    fileInputElem.removeAttr('title');

    $('span.uploading').hide();
};

let startUploading = () => {
    let btnElem = FORM_ELEM.find('button');
    let fileLabelElem = FORM_ELEM.find('label');
    let fileInputElem = FORM_ELEM.find('input[type="file"]');
    let progressElem = $('.progress');
    let progressBar = $('.progress-bar');

    if (btnElem.hasClass('disabled') || fileInputElem.hasClass('disabled') || fileLabelElem.hasClass('disabled')) {
        return false;
    }

    let disabledClassName = 'disabled';
    let titleMessage = 'File is already uploading.';

    btnElem.addClass(disabledClassName);
    btnElem.attr('title', titleMessage);

    fileLabelElem.addClass(disabledClassName);
    fileLabelElem.attr('title', titleMessage);

    fileInputElem.addClass(disabledClassName);
    fileInputElem.attr('title', titleMessage);

    progressElem.show();

    progressBar.addClass('progress-bar-striped');
    progressBar
        .removeClass('progress-bar-danger')
        .removeClass('progress-bar-warning')
        .removeClass('progress-bar-success');

    progressBar.width('0');
    progressBar.attr('aria-valuenow', 0);

    $('span.uploading').show();

    return true;
};

let checkAndSend = (uniqueFileID, fileExtension, uploadedFileSize, reUpload = false) => {
    if (reUpload) {
        $('.alert').remove();
        startUploading();

        $.ajax({
            url: FORM_ELEM.attr('action'),
            method: 'POST',
            async: false,
            data: {
                uniqueFileID: uniqueFileID,
                fileExtension: fileExtension,
                checkFileSize: true
            },
            success: (data) => {
                uploadedFileSize = Number.parseInt(data);
            },
        });
    }

    let progressElem = $('.progress');
    let progressBar = $('.progress-bar');
    let fileName = oneFile.name;
    let fileSize = oneFile.size;

    if (fileSize === uploadedFileSize) {
        progressBar.width('100%');
        progressBar.attr('aria-valuenow', 100);

        progressBar.addClass('progress-bar-success');
        progressBar
            .removeClass('progress-bar-striped')
            .removeClass('progress-bar-warning')
            .removeClass('progress-bar-danger');

        addAlert(TYPE_ALERT_SUCCESS, 'The file was fully downloaded <a href="./files/' + uniqueFileID + '.' + fileExtension +
            '">' + fileName + '</a>.');
        return;
    }

    if (fileSize < uploadedFileSize) {
        progressElem.hide();
        addAlert(TYPE_ALERT_DANGER, 'Uploaded file larger than current.');
        return;
    }

    let progressPercent = (100 * uploadedFileSize) / fileSize;
    let leftSize = fileSize - uploadedFileSize;
    let fileSliceEnd = leftSize > SIZE_CONTENT ? SIZE_CONTENT : leftSize;

    progressBar.width(progressPercent + '%');
    progressBar.attr('aria-valuenow', progressPercent);

    let FR = new FileReader();
    FR.readAsDataURL(oneFile.slice(uploadedFileSize, uploadedFileSize + fileSliceEnd));
    FR.onloadend = () => {
        let fileReaderResult = FR.result;
        if (FR.error) {
            progressElem.hide();

            addAlert(TYPE_ALERT_DANGER, 'Data reading failure.');
            console.error(FR.error);
        } else {
            $.ajax({
                method: 'POST',
                url: FORM_ELEM.attr('action'),
                data: {
                    uniqueFileID: uniqueFileID,
                    fileExtension: fileExtension,
                    fileContent: fileReaderResult,
                },
                success: (data) => {
                    let response = JSON.parse(data);
                    if (response.isOk) {
                        checkAndSend(uniqueFileID, fileExtension, response.uploadedFileSize);
                    } else {
                        progressElem.hide();

                        addAlert(TYPE_ALERT_DANGER, 'Server did not return "success".');
                    }
                },
                error: (jqXHR, status) => {
                    if (status === 'error') {
                        progressBar.addClass('progress-bar-danger');
                        progressBar
                            .removeClass('progress-bar-striped')
                            .removeClass('progress-bar-warning')
                            .removeClass('progress-bar-success');

                        addAlert(TYPE_ALERT_WARNING, 'No internet connection. <span class="re-upload" onclick="checkAndSend(\'' + uniqueFileID + '\',\'' + fileExtension + '\',' + uploadedFileSize + ', true)">Click here</span> to continue uploading.');
                    }
                }
            });
        }
    };
};

jQuery(window).on('load', () => {
    $(document).on('click', '.disabled', (e) => {
        e.preventDefault();
    });

    $(document).on('submit', '#upload-form', (e) => {
        e.preventDefault();

        if (!startUploading()) {
            return false;
        }

        $('.alert').remove();

        let fileElem = $('#file')[0];
        let fileList = fileElem.files;

        if (fileList.length === 0) {
            addAlert(TYPE_ALERT_DANGER, 'File cannot be empty.');
            return;
        }

        if (fileList.length > 1) {
            addAlert(TYPE_ALERT_DANGER, 'The field cannot contain more than one file.');
            return;
        }

        window.oneFile = fileList[0];
        let fileName = oneFile.name;
        let fileSize = oneFile.size;
        let uniqueFileID = fileName + '-' + fileSize + '-' + +oneFile.lastModifiedDate;
        let fileExtension = '';

        let resultSplit = oneFile.name.split('.');
        let lastItem = resultSplit[resultSplit.length - 1];
        if (/^[A-z]+$/.test(lastItem)) {
            fileExtension = lastItem;
        }

        let uploadedFileSize = 0;
        $.ajax({
            url: FORM_ELEM.attr('action'),
            method: 'POST',
            data: {
                uniqueFileID: uniqueFileID,
                fileExtension: fileExtension,
                checkFileSize: true
            },
            success: (data) => {
                uploadedFileSize = Number.parseInt(data);
                checkAndSend(uniqueFileID, fileExtension, uploadedFileSize);
            },
        });
    });

    $(document).on('change', '#file', () => {
        $('.alert').remove();
        $('.progress').hide();
    });
});
